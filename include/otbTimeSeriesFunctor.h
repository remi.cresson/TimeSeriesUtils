/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include <array>
namespace otb
{

namespace Functor
{

/** \class MultitemporalDensityFunctor
 *  \brief This functor computes the number of images in the input stack
 *
 *  \ingroup Functor
 */
template <class TInputPixel, class TOutputLabel>
class MultitemporalDensityFunctor
{
  typedef typename TInputPixel::ValueType InputValueType;
  typedef typename TOutputLabel::ValueType OutputLabelValueType;

public:

  /*
   * Pixel components:
   * 1: number of images
   */
  static constexpr std::size_t outputPixelSize{1};

  std::size_t OutputSize(const std::array<size_t, 1>&) const
  {
    return outputPixelSize;
  }

  std::size_t OutputSize() const
  {
    return outputPixelSize;
  }

  // Constructor
  MultitemporalDensityFunctor() {

    // input no-data value
    m_InputNoDataValue = 0;
  }

  // Destructor
  virtual ~MultitemporalDensityFunctor(){}

  // No data set/get
  void SetInputNoDataValue(InputValueType value) { m_InputNoDataValue = value; }
  InputValueType GetInputNoDataValue() { return m_InputNoDataValue; }

  /*
   * Compute the output pixel
   */
  inline TOutputLabel operator()(const TInputPixel & pixel) const
  {

    // Output pixel
    TOutputLabel outLabel;
    outLabel.SetSize(OutputSize());
    outLabel.Fill(0);

    int n = pixel.GetSize();

    // Comput nb. of valid pixels
    for (int i = 0 ; i < n; i++)
      {
        if (pixel[i] != m_InputNoDataValue)
          outLabel[0]++;
      }

    return outLabel;
  }

private:

  // no data values
  InputValueType m_InputNoDataValue;


}; // MultitemporalDensityFunctor


}

}

/*=========================================================================

  Program:   TimeSeriesUtils
  Language:  C++

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.

  See LICENSE for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef MODULES_REMOTE_TIMESERIESUTILS_INCLUDE_OTBDATES_H_
#define MODULES_REMOTE_TIMESERIESUTILS_INCLUDE_OTBDATES_H_


#include "boost/date_time/gregorian/gregorian.hpp"
#include "boost/math/distributions/students_t.hpp"
#include <fstream>
using std::ifstream;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iterator>
using std::istream_iterator;
#include <algorithm>
using std::copy;
#include "itkMacro.h"

namespace otb
{

namespace dates
{

/*
 * \class SingleDate
 *
 * \brief Class to handle dates. Julian day is computed at instantiation. Uses boost.
 * \see boost::gregorian::date
 *
 * \ingroup TimeSeriesUtils
 */
class SingleDate
{
public:
  SingleDate(int yyyy, int mm, int dd){
    year = yyyy;
    day = dd;
    month = mm;

    // Compute julianday
    boost::gregorian::date d(yyyy, mm, dd);
    julianday = d.julian_day();
  }

  ~SingleDate (){}

  /*
   * Return true if the date is in the time range given by (startmonth; startday; endmonth, endday)
   */
  bool IsInRange(const int& startMonth, const int& startDay, const int& endMonth, const int& endDay) const
  {
    if (startMonth < month && month < endMonth)
      {
        return true;
      }
    else if (startMonth == month && startDay <= day)
      {
        return true;
      }
    else if (endMonth == month && day <= endDay)
      {
        return true;
      }
    else
      {
        return false;
      }
  }

  /*
   * Return true if the date is in the time range given by (starDate, nbOfDays)
   */
  bool IsInRange(SingleDate startDate, const int& nbOfDays) const
  {
    return (startDate.julianday <= julianday && julianday <= startDate.julianday + nbOfDays);
  }

  int day;
  int month;
  int year;
  int julianday;
};

typedef std::vector<SingleDate> DatesType;

/*
 * Retrieve the vector of SingleDate from the input vector of strings.
 * Input string must be in the form of "YYYYMMDD ....."
 */
DatesType GetDatesFromStringVector(std::vector<std::string> & list, bool check_order = true){

  DatesType dates;
  for (unsigned int i = 0 ; i < list.size() ; i++)
    {
      if (list[i].size() >= 8) // YYYYMMDD
        {
          int yyyy, mm, dd;
          try
          {
              yyyy = std::stoi( list[i].substr(0,4) );
              mm   = std::stoi( list[i].substr(4,2) );
              dd   = std::stoi( list[i].substr(6,2) );
          }
          catch (...)
          {
              itkGenericExceptionMacro("Dates format must be YYYYMMDD");
          }

          // Add new date
          // The boost::gregorian::date(yyyy, mm, dd) takes care of
          // checking the date validity
          SingleDate d(yyyy, mm, dd);
          dates.push_back(d);
        }
    }

  // we need at least 2 dates !
  if (dates.size()<2)
    {
      itkGenericExceptionMacro("Must be at least 2 dates to work");
    }

  // Check dates ordering
  if (check_order)
    {
      for (unsigned int i = 0 ; i < dates.size()-1 ; i++)
        {
          if (dates[i+1].julianday < dates[i].julianday)
            {
              itkGenericExceptionMacro("Date " << i << " is older than date " << (i+1) << " !");
            }
        }
    }

  // return output
  return dates;
}

/*
 * Retrieve the vector of dates (as string) from an ASCII file.
 * Format of dates must be YYYYMMDD
 */
std::vector<std::string> GetDatesAsStringFromFile(std::string filename)
{
  std::vector<std::string> list;
  ifstream file(filename);
  if (!file.is_open())
    {
      itkGenericExceptionMacro("Can not open file " << filename);
    }

  std::copy(std::istream_iterator<std::string>(file),
      std::istream_iterator<std::string>(),
      std::back_inserter(list));

  return list;
}

/*
 * Retrieve the vector of dates from an ASCII file.
 * Format of dates must be YYYYMMDD
 */
DatesType GetDatesFromFile(std::string filename)
{

  std::vector<std::string> list = GetDatesAsStringFromFile(filename);

  return GetDatesFromStringVector(list);

}


} // namespace dates

} // namespace otb

#endif /* MODULES_REMOTE_TIMESERIESUTILS_INCLUDE_OTBDATES_H_ */

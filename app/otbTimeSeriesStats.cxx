/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#include "itkFixedArray.h"
#include "itkObjectFactory.h"
#include "otbWrapperApplicationFactory.h"

// Application engine
#include "otbStandardFilterWatcher.h"
#include "itkFixedArray.h"

// LayerStack
#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"

// Extract multichannel
#include "otbMultiChannelExtractROI.h"

// Input images list
#include "otbWrapperInputImageListParameter.h"

// Functors for NDVI operations
#include "otbFunctorImageFilter.h"
#include "otbTimeSeriesFunctor.h"

// Dates
#include "otbDates.h"

namespace otb
{

namespace Wrapper
{

class TimeSeriesStats : public Application
{
public:
  /** Standard class typedefs. */
  typedef TimeSeriesStats            Self;
  typedef Application                                Superclass;
  typedef itk::SmartPointer<Self>                    Pointer;
  typedef itk::SmartPointer<const Self>              ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(TimeSeriesStats, Application);

  /** Typedefs for image concatenation */
  typedef otb::ImageList<FloatImageType>                                        ImageListType;
  typedef ImageListToVectorImageFilter<ImageListType, FloatVectorImageType>     ListConcatenerFilterType;
  typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
      FloatImageType::PixelType>                                                ExtractROIFilterType;
  typedef ObjectList<ExtractROIFilterType>                                      ExtractROIFilterListType;

  /** Typedefs for temporal correlation */
  typedef unsigned char                                                         LabelValueType;
  typedef otb::VectorImage<LabelValueType>                                      LabelVectorImageType;
  typedef otb::dates::SingleDate                                                DateType;
  typedef otb::Functor::MultitemporalDensityFunctor<FloatVectorImageType::PixelType,
      LabelVectorImageType::PixelType>                                          DensityFunctorType;
  typedef otb::FunctorImageFilter<DensityFunctorType>                           DensityFilterType;

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoInit()
  {

    // Documentation
    SetName("TimeSeriesStats");
    SetDescription("Compute time series statistics");
    SetDocLongDescription("This application computes time series statistics. The first component of the output image is the number of images different than no-data.");
    SetDocLimitations("None");
    SetDocAuthors("Remi Cresson");

    // Input time series images
    AddParameter(ParameterType_InputImageList, "il", "Input time series");
    SetParameterDescription                   ("il", "The user can specify multiple input images, or one big stack of images.");

    // Input nodata
    AddParameter(ParameterType_Float, "nodata", "Input no-data value");
    MandatoryOff                     ("nodata");
    SetDefaultParameterFloat         ("nodata", 0.0);

    // Output image
    AddParameter(ParameterType_OutputImage, "out", "Output label image");
    SetParameterDescription                ("out", "Output label image");
    SetDefaultOutputPixelType              ("out", ImagePixelType_uint8);

    AddRAMParameter();

  }

  /*
   * Execute the process
   */
  void DoExecute()
  {
    // Get the input image list
    FloatVectorImageListType::Pointer inList = this->GetParameterImageList("il");

    if( inList->Size() < 2 )
      {
        otbAppLogFATAL("At least two images are required.");
      }
    if ( inList->Size() >= itk::NumericTraits<LabelValueType>::max())
      {
        otbAppLogFATAL("The maximum number of input images must be below " <<
            itk::NumericTraits<LabelValueType>::max() << ". Consider changing LabelValueType.");
      }

    // Create one stack for input NDVI images list
    m_Concatener = ListConcatenerFilterType::New();
    m_ImageList = ImageListType::New();
    m_ExtractorList = ExtractROIFilterListType::New();

    // Split each input vector image into image
    // and generate an mono channel image list
    inList->GetNthElement(0)->UpdateOutputInformation();
    FloatVectorImageType::SizeType size = inList->GetNthElement(0)->GetLargestPossibleRegion().GetSize();
    for( unsigned int i=0; i<inList->Size(); i++ )
      {
        FloatVectorImageType::Pointer vectIm = inList->GetNthElement(i);
        vectIm->UpdateOutputInformation();
        if( size != vectIm->GetLargestPossibleRegion().GetSize() )
          {
            itkExceptionMacro("Input Image size mismatch...");
          }

        for( unsigned int j=0; j<vectIm->GetNumberOfComponentsPerPixel(); j++)
          {
            ExtractROIFilterType::Pointer extractor = ExtractROIFilterType::New();
            extractor->SetInput( vectIm );
            extractor->SetChannel( j+1 );
            extractor->UpdateOutputInformation();
            m_ExtractorList->PushBack( extractor );
            m_ImageList->PushBack( extractor->GetOutput() );
          }
      }

    // Concatenate the images
    m_Concatener->SetInput( m_ImageList );
    m_Concatener->UpdateOutputInformation();

    // Count images
    m_TemporalFilter = DensityFilterType::New();
    m_TemporalFilter->SetInput(m_Concatener->GetOutput());
    m_TemporalFilter->GetModifiableFunctor().SetInputNoDataValue(GetParameterFloat("nodata"));

    SetParameterOutputImage("out", m_TemporalFilter->GetOutput());

} // DoExecute()

  ListConcatenerFilterType::Pointer         m_Concatener;
  ExtractROIFilterListType::Pointer         m_ExtractorList;
  ImageListType::Pointer                    m_ImageList;
  DensityFilterType::Pointer                m_TemporalFilter;

}; // end of class

} // namespace wrapper
} // namespace otb

OTB_APPLICATION_EXPORT( otb::Wrapper::TimeSeriesStats )

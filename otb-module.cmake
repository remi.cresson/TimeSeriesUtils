set(DOCUMENTATION "Time Series Utils")

otb_module(TimeSeriesUtils
  DEPENDS
	OTBIndices
	OTBStatistics
    OTBApplicationEngine
    	
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
  DESCRIPTION
    "Useful functions to help processing time series"
)
